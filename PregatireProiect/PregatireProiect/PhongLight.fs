#version 330 core
out vec4 FragColor;

in vec3 Normal;  
in vec3 FragPos;  
  
uniform vec3 lightPos; 
uniform vec3 viewPos; 
uniform vec3 lightColor;
uniform vec3 objectColor;
uniform float Ka;
uniform float Kd;
uniform float n;

void main()
{
	
	vec3 norm = normalize(Normal);
	vec3 LightDir = normalize(lightPos - FragPos);
	vec3 diff = Kd * max(dot(norm, LightDir), 0.0) * lightColor; 
	vec3 ambient = Ka* lightColor;
	vec3 result = (ambient + diff) * objectColor;
	vec3 viewDir = normalize(viewPos - FragPos);
	vec3 reflectDir = reflect(-LightDir, norm);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), n);
    FragColor = vec4(result, 1.0);
}