#pragma once

#include <glm/gtc/matrix_transform.hpp>

#include <vector>

#include "Shader.h"

using namespace std;

struct Vertex {
	// position
	glm::vec3 Position;
	// normal
	glm::vec3 Normal;
	// texCoords
	glm::vec2 TexCoords;
};

struct Texture {
	unsigned int id;
	string type;
	string path;
};

struct Material {
	glm::vec3 specular;
	glm::vec3 ambient;
	glm::vec3 diffuse;
	float specularExponent;
	Material(const glm::vec3& amb, const glm::vec3& diff, const glm::vec3& spec, float exponent);
};

class Mesh {
public:
	/*  Mesh Data  */
	vector<Vertex> vertices;
	vector<unsigned int> indices;
	vector<Texture> textures;
	unsigned int VAO;

	Mesh(vector<Vertex> vertices, vector<unsigned int> indices, vector<Texture> textures);

	// render the mesh
	void Draw(Shader shader);
private:
	unsigned int VBO, EBO;
private:
	// initializes all the buffer objects/arrays
	void setupMesh();
};